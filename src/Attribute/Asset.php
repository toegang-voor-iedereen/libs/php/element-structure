<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

class Asset
{
    /**
     * @param string $filename
     * @param string $contentLocation
     */
    public function __construct(
        protected string $filename,
        protected string $contentLocation
    ) {
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return void
     */
    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getContentLocation(): string
    {
        return $this->contentLocation;
    }

    /**
     * @param string $contentLocation
     * @return void
     */
    public function setContentLocation(string $contentLocation): void
    {
        $this->contentLocation = $contentLocation;
    }
}
