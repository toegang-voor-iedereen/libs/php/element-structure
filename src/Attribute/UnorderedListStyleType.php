<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

enum UnorderedListStyleType: string
{
    case Disc = 'disc';
    case Circle = 'circle';
    case Square = 'square';
}
