<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

class UnorderedListElementAttributes
{
    /**
     * @param UnorderedListStyleType $styleType
     */
    public function __construct(
        protected UnorderedListStyleType $styleType = UnorderedListStyleType::Circle
    ) {
    }

    /**
     * @return UnorderedListStyleType
     */
    public function getStyleType(): UnorderedListStyleType
    {
        return $this->styleType;
    }

    /**
     * @param UnorderedListStyleType $styleType
     * @return void
     */
    public function setStyleType(UnorderedListStyleType $styleType): void
    {
        $this->styleType = $styleType;
    }
}
