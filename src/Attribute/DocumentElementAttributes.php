<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

class DocumentElementAttributes
{
    /**
     * @param string|null $title
     * @param string|null $subtitle
     * @param string|null $titlePrefix
     * @param string|null $language
     * @param string|null $direction
     * @param string|null $category
     * @param string|null $date
     * @param string[] $keywords
     * @param string[] $authors
     * @param Asset[] $assets
     */
    public function __construct(
        protected ?string $title = null,
        protected ?string $subtitle = null,
        protected ?string $titlePrefix = null,
        protected ?string $language = null,
        protected ?string $direction = null,
        protected ?string $category = null,
        protected ?string $date = null,
        protected array $keywords = [],
        protected array $authors = [],
        protected array $assets = []
    ) {
    }

    /**
     * @return Asset[]
     */
    public function getAssets(): array
    {
        return $this->assets;
    }

    /**
     * @param Asset[] $assets
     * @return void
     */
    public function setAssets(array $assets): void
    {
        $this->assets = $assets;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return void
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param string|null $subtitle
     * @return void
     */
    public function setSubtitle(?string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string|null
     */
    public function getTitlePrefix(): ?string
    {
        return $this->titlePrefix;
    }

    /**
     * @param string|null $titlePrefix
     * @return void
     */
    public function setTitlePrefix(?string $titlePrefix): void
    {
        $this->titlePrefix = $titlePrefix;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     * @return void
     */
    public function setLanguage(?string $language): void
    {
        $this->language = $language;
    }

    /**
     * @return string|null
     */
    public function getDirection(): ?string
    {
        return $this->direction;
    }

    /**
     * @param string|null $direction
     * @return void
     */
    public function setDirection(?string $direction): void
    {
        $this->direction = $direction;
    }

    /**
     * @return string|null
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string|null $category
     * @return void
     */
    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string[]
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param string[] $keywords
     * @return void
     */
    public function setKeywords(array $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string[]
     */
    public function getAuthors(): array
    {
        return $this->authors;
    }

    /**
     * @param string[] $authors
     * @return void
     */
    public function setAuthors(array $authors): void
    {
        $this->authors = $authors;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return void
     */
    public function setDate(?string $date): void
    {
        $this->date = $date;
    }
}
