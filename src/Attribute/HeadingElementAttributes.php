<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

class HeadingElementAttributes
{
    /**
     * @param int $level
     */
    public function __construct(
        protected int $level,
    ) {
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return void
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }
}
