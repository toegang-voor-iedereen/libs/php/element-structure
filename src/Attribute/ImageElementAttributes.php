<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

class ImageElementAttributes
{
    /**
     * @param string|null $src
     * @param string $alt
     */
    public function __construct(
        protected ?string $src,
        protected string $alt,
    ) {
    }

    /**
     * @return string|null
     */
    public function getSrc(): ?string
    {
        return $this->src;
    }

    /**
     * @param string|null $src
     * @return void
     */
    public function setSrc(?string $src): void
    {
        $this->src = $src;
    }

    /**
     * @return string
     */
    public function getAlt(): string
    {
        return $this->alt;
    }

    /**
     * @param string $alt
     * @return void
     */
    public function setAlt(string $alt): void
    {
        $this->alt = $alt;
    }
}
