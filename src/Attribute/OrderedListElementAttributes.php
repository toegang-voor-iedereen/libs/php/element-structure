<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

class OrderedListElementAttributes
{
    /**
     * @param OrderedListStyleType $styleType
     * @param int $start
     * @param bool $reversed
     */
    public function __construct(
        protected OrderedListStyleType $styleType = OrderedListStyleType::Decimal,
        protected int $start = 1,
        protected bool $reversed = false
    ) {
    }

    /**
     * @return OrderedListStyleType
     */
    public function getStyleType(): OrderedListStyleType
    {
        return $this->styleType;
    }

    /**
     * @param OrderedListStyleType $styleType
     * @return void
     */
    public function setStyleType(OrderedListStyleType $styleType): void
    {
        $this->styleType = $styleType;
    }

    /**
     * @return int
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @param int $start
     * @return void
     */
    public function setStart(int $start): void
    {
        $this->start = $start;
    }

    /**
     * @return bool
     */
    public function isReversed(): bool
    {
        return $this->reversed;
    }

    /**
     * @param bool $reversed
     * @return void
     */
    public function setReversed(bool $reversed): void
    {
        $this->reversed = $reversed;
    }
}
