<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Attribute;

enum OrderedListStyleType: string
{
    case Decimal = 'decimal';
    case AlphaLower = 'alpha-lower';
    case AlphaUpper = 'alpha-upper';
    case RomanLower = 'roman-lower';
    case RomanUpper = 'roman-upper';
}
