<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\UnorderedListElementAttributes;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;

class UnorderedListElement implements RootElementInterface
{
    /**
     * @param string $identifier
     * @param ListMemberElement[] $members
     * @param UnorderedListElementAttributes $attributes
     * @param ValidationMessage[] $validation
     */
    public function __construct(
        protected string $identifier,
        protected array $members = [],
        protected UnorderedListElementAttributes $attributes = new UnorderedListElementAttributes(),
        protected array $validation = [],
    ) {
        $this->members = $members;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return void
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return UnorderedListElementAttributes
     */
    public function getAttributes(): UnorderedListElementAttributes
    {
        return $this->attributes;
    }

    /**
     * @param UnorderedListElementAttributes $attributes
     * @return void
     */
    public function setAttributes(UnorderedListElementAttributes $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return ValidationMessage[]
     */
    public function getValidation(): array
    {
        return $this->validation;
    }

    /**
     * @param ValidationMessage[] $validation
     * @return void
     */
    public function setValidation(array $validation): void
    {
        $this->validation = $validation;
    }

    /**
     * @param ValidationMessage $validation
     */
    public function addValidation(ValidationMessage $validation): void
    {
        $this->validation[] = $validation;
    }

    /**
     * @param ValidationMessage[] $validation
     */
    public function addValidations(array $validation): void
    {
        $this->validation = array_merge($this->validation, $validation);
    }

    /**
     * @param ListMemberElement $member
     * @return void
     */
    public function addMember(ListMemberElement $member): void
    {
        $this->members[] = $member;
    }

    /**
     * @param ListMemberElement[] $members
     * @return void
     */
    public function addMembers(array $members): void
    {
        $this->members = array_merge($this->members, $members);
    }

    /**
     * @return ListMemberElement[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }

    /**
     * @param ListMemberElement[] $members
     * @return void
     */
    public function setMembers(array $members): void
    {
        $this->members = $members;
    }
}
