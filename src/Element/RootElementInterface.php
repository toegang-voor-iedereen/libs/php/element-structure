<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Element;

interface RootElementInterface extends ElementInterface
{
}
