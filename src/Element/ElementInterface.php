<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Validation\ValidationMessage;

interface ElementInterface
{
    public function getIdentifier(): string;

    public function setIdentifier(string $identifier): void;

    /**
     * @return ValidationMessage[]
     */
    public function getValidation(): array;

    /**
     * @param ValidationMessage[] $validation
     * @return void
     */
    public function setValidation(array $validation): void;

    /**
     * @param ValidationMessage $validation
     */
    public function addValidation(ValidationMessage $validation): void;

    /**
     * @param ValidationMessage[] $validation
     */
    public function addValidations(array $validation): void;
}
