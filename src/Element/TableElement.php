<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\EmptyAttributes;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;

class TableElement implements RootElementInterface
{
    /**
     * @param string $identifier
     * @param string|null $content
     * @param EmptyAttributes $attributes
     * @param ValidationMessage[] $validation
     */
    public function __construct(
        protected string $identifier,
        protected ?string $content = null,
        protected EmptyAttributes $attributes = new EmptyAttributes(),
        protected array $validation = [],
    ) {
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return void
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }



    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return void
     */
    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return EmptyAttributes
     */
    public function getAttributes(): EmptyAttributes
    {
        return $this->attributes;
    }

    /**
     * @param EmptyAttributes $attributes
     * @return void
     */
    public function setAttributes(EmptyAttributes $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return ValidationMessage[]
     */
    public function getValidation(): array
    {
        return $this->validation;
    }

    /**
     * @param ValidationMessage[] $validation
     * @return void
     */
    public function setValidation(array $validation): void
    {
        $this->validation = $validation;
    }

    /**
     * @param ValidationMessage $validation
     */
    public function addValidation(ValidationMessage $validation): void
    {
        $this->validation[] = $validation;
    }

    /**
     * @param ValidationMessage[] $validation
     */
    public function addValidations(array $validation): void
    {
        $this->validation = array_merge($this->validation, $validation);
    }
}
