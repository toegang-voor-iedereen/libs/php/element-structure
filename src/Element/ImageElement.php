<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\ImageElementAttributes;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;

class ImageElement implements RootElementInterface
{
    /**
     * @param string $identifier
     * @param ImageElementAttributes $attributes
     * @param ValidationMessage[] $validation
     */
    public function __construct(
        protected string $identifier,
        protected ImageElementAttributes $attributes,
        protected array $validation = [],
    ) {
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return void
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return ImageElementAttributes
     */
    public function getAttributes(): ImageElementAttributes
    {
        return $this->attributes;
    }

    /**
     * @param ImageElementAttributes $attributes
     * @return void
     */
    public function setAttributes(ImageElementAttributes $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return ValidationMessage[]
     */
    public function getValidation(): array
    {
        return $this->validation;
    }

    /**
     * @param ValidationMessage[] $validation
     * @return void
     */
    public function setValidation(array $validation): void
    {
        $this->validation = $validation;
    }

    /**
     * @param ValidationMessage $validation
     */
    public function addValidation(ValidationMessage $validation): void
    {
        $this->validation[] = $validation;
    }

    /**
     * @param ValidationMessage[] $validation
     */
    public function addValidations(array $validation): void
    {
        $this->validation = array_merge($this->validation, $validation);
    }
}
