<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\DocumentElementAttributes;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;

class DocumentElement implements ElementInterface
{
    /**
     * @param string $identifier
     * @param RootElementInterface[] $members
     * @param DocumentElementAttributes $attributes
     * @param ValidationMessage[] $validation
     */
    public function __construct(
        protected string $identifier,
        protected array $members = [],
        protected DocumentElementAttributes $attributes = new DocumentElementAttributes(),
        protected array $validation = [],
    ) {
        $this->members = $members;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return void
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return DocumentElementAttributes
     */
    public function getAttributes(): DocumentElementAttributes
    {
        return $this->attributes;
    }

    /**
     * @param DocumentElementAttributes $attributes
     * @return void
     */
    public function setAttributes(DocumentElementAttributes $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return ValidationMessage[]
     */
    public function getValidation(): array
    {
        return $this->validation;
    }

    /**
     * @param ValidationMessage[] $validation
     * @return void
     */
    public function setValidation(array $validation): void
    {
        $this->validation = $validation;
    }

    /**
     * @param ValidationMessage $validation
     */
    public function addValidation(ValidationMessage $validation): void
    {
        $this->validation[] = $validation;
    }

    /**
     * @param ValidationMessage[] $validation
     */
    public function addValidations(array $validation): void
    {
        $this->validation = array_merge($this->validation, $validation);
    }

    /**
     * @param RootElementInterface $member
     * @return void
     */
    public function addMember(RootElementInterface $member): void
    {
        $this->members[] = $member;
    }

    /**
     * @param RootElementInterface[] $members
     * @return void
     */
    public function addMembers(array $members): void
    {
        $this->members = array_merge($this->members, $members);
    }

    /**
     * @return RootElementInterface[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }

    /**
     * @param RootElementInterface[] $members
     * @return void
     */
    public function setMembers(array $members): void
    {
        $this->members = $members;
    }
}
