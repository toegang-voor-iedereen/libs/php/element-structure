<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Validation;

enum ValidationMessageState: string
{
    case Ignored = 'ignored';
    case Resolved = 'resolved';
    case Unresolved = 'unresolved';
}
