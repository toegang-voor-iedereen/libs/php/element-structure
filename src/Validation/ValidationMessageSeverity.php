<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Validation;

enum ValidationMessageSeverity: string
{
    case Info = 'info';
    case Warning = 'warning';
    case Error = 'error';
}
