<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Validation;

class ValidationMessage
{
    /**
     * @param string $identifier
     * @param ValidationMessageSeverity $severity
     * @param string $message
     * @param string $code
     * @param ValidationMessageContext $context
     * @param Checksum $checksum
     * @param ValidationMessageState $state
     */
    public function __construct(
        protected string $identifier,
        protected ValidationMessageSeverity $severity,
        protected string $message,
        protected string $code,
        protected ValidationMessageContext $context,
        protected Checksum $checksum,
        protected ValidationMessageState $state
    ) {
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return void
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return ValidationMessageSeverity
     */
    public function getSeverity(): ValidationMessageSeverity
    {
        return $this->severity;
    }

    /**
     * @param ValidationMessageSeverity $severity
     * @return void
     */
    public function setSeverity(ValidationMessageSeverity $severity): void
    {
        $this->severity = $severity;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return void
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return void
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return ValidationMessageContext
     */
    public function getContext(): ValidationMessageContext
    {
        return $this->context;
    }

    /**
     * @param ValidationMessageContext $context
     * @return void
     */
    public function setContext(ValidationMessageContext $context): void
    {
        $this->context = $context;
    }

    /**
     * @return Checksum
     */
    public function getChecksum(): Checksum
    {
        return $this->checksum;
    }

    /**
     * @param Checksum $checksum
     * @return void
     */
    public function setChecksum(Checksum $checksum): void
    {
        $this->checksum = $checksum;
    }

    /**
     * @return ValidationMessageState
     */
    public function getState(): ValidationMessageState
    {
        return $this->state;
    }

    /**
     * @param ValidationMessageState $state
     * @return void
     */
    public function setState(ValidationMessageState $state): void
    {
        $this->state = $state;
    }
}
