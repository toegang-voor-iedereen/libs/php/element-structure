<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Validation;

class Checksum
{
    /**
     * @param HashAlgorithmType $algorithm
     * @param string $hash
     */
    public function __construct(
        protected HashAlgorithmType $algorithm,
        protected string $hash,
    ) {
    }

    /**
     * @return HashAlgorithmType
     */
    public function getAlgorithm(): HashAlgorithmType
    {
        return $this->algorithm;
    }

    /**
     * @param HashAlgorithmType $algorithm
     * @return void
     */
    public function setAlgorithm(HashAlgorithmType $algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return void
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }
}
