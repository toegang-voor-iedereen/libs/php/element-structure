<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Validation;

class ValidationMessageContext
{
    /**
     * @param string|null $selector
     */
    public function __construct(
        protected ?string $selector = null,
    ) {
    }

    /**
     * @return string|null
     */
    public function getSelector(): ?string
    {
        return $this->selector;
    }

    /**
     * @param string|null $selector
     * @return void
     */
    public function setSelector(?string $selector): void
    {
        $this->selector = $selector;
    }
}
