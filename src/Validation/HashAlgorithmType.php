<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Types\Validation;

enum HashAlgorithmType: string
{
    case BLAKE2b512 = 'blake2b512';
    case SHA1 = 'sha1';
    case SHA512 = 'sha512';
    case SHA3_512 = 'sha3-512';
}
