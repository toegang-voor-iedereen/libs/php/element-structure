<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Validation;

use NLdoc\ElementStructure\Types\Validation\Checksum;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use NLdoc\ElementStructure\Types\Validation\ValidationMessageContext;
use NLdoc\ElementStructure\Types\Validation\ValidationMessageSeverity;
use NLdoc\ElementStructure\Types\Validation\ValidationMessageState;
use PHPUnit\Framework\Attributes\CoversClass;
use Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(ValidationMessage::class)]
class ValidationMessageTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructor(): void
    {
        $checksum = $this->createMock(Checksum::class);
        $context = $this->createMock(ValidationMessageContext::class);

        $message = new ValidationMessage(
            "test-id",
            ValidationMessageSeverity::Info,
            "test-message",
            "test-code",
            $context,
            $checksum,
            ValidationMessageState::Resolved,
        );

        $this->assertSame("test-id", $message->getIdentifier());
        $this->assertSame(ValidationMessageSeverity::Info, $message->getSeverity());
        $this->assertSame("test-message", $message->getMessage());
        $this->assertSame("test-code", $message->getCode());
        $this->assertSame($context, $message->getContext());
        $this->assertSame($checksum, $message->getChecksum());
        $this->assertSame(ValidationMessageState::Resolved, $message->getState());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $checksum = $this->createMock(Checksum::class);
        $context = $this->createMock(ValidationMessageContext::class);

        $message = new ValidationMessage(
            "test-id",
            ValidationMessageSeverity::Info,
            "test-message",
            "test-code",
            $context,
            $checksum,
            ValidationMessageState::Resolved,
        );

        $newContext = $this->createMock(ValidationMessageContext::class);
        $newChecksum = $this->createMock(Checksum::class);

        $message->setIdentifier("new-id");
        $message->setSeverity(ValidationMessageSeverity::Warning);
        $message->setMessage("new-message");
        $message->setCode("new-code");
        $message->setContext($newContext);
        $message->setChecksum($newChecksum);
        $message->setState(ValidationMessageState::Unresolved);

        $this->assertSame("new-id", $message->getIdentifier());
        $this->assertSame(ValidationMessageSeverity::Warning, $message->getSeverity());
        $this->assertSame("new-message", $message->getMessage());
        $this->assertSame("new-code", $message->getCode());
        $this->assertSame($newContext, $message->getContext());
        $this->assertSame($newChecksum, $message->getChecksum());
        $this->assertSame(ValidationMessageState::Unresolved, $message->getState());
    }
}
