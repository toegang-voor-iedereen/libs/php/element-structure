<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Validation;

use NLdoc\ElementStructure\Types\Validation\Checksum;
use NLdoc\ElementStructure\Types\Validation\HashAlgorithmType;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(Checksum::class)]
class ChecksumTest extends TestCase
{
    public function testConstructing(): void
    {
        $element = new Checksum(HashAlgorithmType::SHA1, "test-hash");
        $this->assertEquals(HashAlgorithmType::SHA1, $element->getAlgorithm());
        $this->assertEquals("test-hash", $element->getHash());
    }

    public function testSetters(): void
    {
        $element = new Checksum(HashAlgorithmType::SHA1, "test-hash");
        $element->setAlgorithm(HashAlgorithmType::SHA3_512);
        $element->setHash("new-hash");
        $this->assertEquals(HashAlgorithmType::SHA3_512, $element->getAlgorithm());
        $this->assertEquals("new-hash", $element->getHash());
    }
}
