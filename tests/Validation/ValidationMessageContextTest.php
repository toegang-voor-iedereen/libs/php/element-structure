<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Validation;

use NLdoc\ElementStructure\Types\Validation\ValidationMessageContext;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(ValidationMessageContext::class)]
class ValidationMessageContextTest extends TestCase
{
    public function testConstructor(): void
    {
        $context = new ValidationMessageContext("test-selector");
        $this->assertEquals("test-selector", $context->getSelector());
    }

    public function testSetters(): void
    {
        $context = new ValidationMessageContext("test-selector");
        $context->setSelector("new-selector");
        $this->assertEquals("new-selector", $context->getSelector());
    }
}
