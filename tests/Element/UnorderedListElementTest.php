<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\OrderedListElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\UnorderedListElementAttributes;
use NLdoc\ElementStructure\Types\Element\ListMemberElement;
use NLdoc\ElementStructure\Types\Element\OrderedListElement;
use NLdoc\ElementStructure\Types\Element\UnorderedListElement;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use PHPUnit\Framework\Attributes\CoversClass;
use Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(UnorderedListElement::class)]
class UnorderedListElementTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructing(): void
    {
        $attributes = $this->createMock(UnorderedListElementAttributes::class);
        $member = $this->createMock(ListMemberElement::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new UnorderedListElement(
            "test-id",
            [$member],
            $attributes,
            [$validationMessage]
        );
        $this->assertSame("test-id", $element->getIdentifier());
        $this->assertSame([$member], $element->getMembers());
        $this->assertSame($attributes, $element->getAttributes());
        $this->assertSame([$validationMessage], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $attributes = $this->createMock(UnorderedListElementAttributes::class);
        $member = $this->createMock(ListMemberElement::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new UnorderedListElement(
            "test-id",
            [$member],
            $attributes,
            [$validationMessage]
        );

        $attributes2 = $this->createMock(UnorderedListElementAttributes::class);
        $member2 = $this->createMock(ListMemberElement::class);
        $validationMessage2 = $this->createMock(ValidationMessage::class);
        $element->setIdentifier("new-id");
        $element->setMembers([$member2]);
        $element->setAttributes($attributes2);
        $element->setValidation([$validationMessage2]);

        $this->assertSame("new-id", $element->getIdentifier());
        $this->assertSame($attributes2, $element->getAttributes());
        $this->assertSame([$member2], $element->getMembers());
        $this->assertSame([$validationMessage2], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidation(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);

        $element = new UnorderedListElement(
            "test-id",
            [],
            $this->createMock(UnorderedListElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidation($validationMessageTwo);

        $this->assertSame([$validationMessageOne, $validationMessageTwo], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidations(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);
        $validationMessageThree = $this->createMock(ValidationMessage::class);

        $element = new UnorderedListElement(
            "test-id",
            [],
            $this->createMock(UnorderedListElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidations([$validationMessageTwo, $validationMessageThree]);

        $this->assertSame(
            [$validationMessageOne, $validationMessageTwo, $validationMessageThree],
            $element->getValidation()
        );
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddMember(): void
    {
        $memberOne = $this->createMock(ListMemberElement::class);
        $memberTwo = $this->createMock(ListMemberElement::class);

        $element = new UnorderedListElement(
            "test-id",
            [$memberOne],
            $this->createMock(UnorderedListElementAttributes::class),
            []
        );

        $this->assertSame([$memberOne], $element->getMembers());

        $element->addMember($memberTwo);

        $this->assertSame([$memberOne, $memberTwo], $element->getMembers());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddMembers(): void
    {
        $memberOne = $this->createMock(ListMemberElement::class);
        $memberTwo = $this->createMock(ListMemberElement::class);
        $memberThree = $this->createMock(ListMemberElement::class);

        $element = new UnorderedListElement(
            "test-id",
            [$memberOne],
            $this->createMock(UnorderedListElementAttributes::class),
            []
        );

        $this->assertSame([$memberOne], $element->getMembers());

        $element->addMembers([$memberTwo, $memberThree]);

        $this->assertSame(
            [$memberOne, $memberTwo, $memberThree],
            $element->getMembers()
        );
    }
}
