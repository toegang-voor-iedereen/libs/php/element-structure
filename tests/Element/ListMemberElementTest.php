<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\EmptyAttributes;
use NLdoc\ElementStructure\Types\Attribute\ImageElementAttributes;
use NLdoc\ElementStructure\Types\Element\ImageElement;
use NLdoc\ElementStructure\Types\Element\ListMemberElement;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use PHPUnit\Framework\Attributes\CoversClass;
use Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(ListMemberElement::class)]
class ListMemberElementTest extends TestCase
{
  /**
   * @throws Exception
   */
    public function testConstructing(): void
    {
        $validationMessage = $this->createMock(ValidationMessage::class);
        $element = new ListMemberElement(
            "test-id",
            "test-content",
            new EmptyAttributes(),
            [$validationMessage]
        );
        $this->assertSame("test-id", $element->getIdentifier());
        $this->assertSame("test-content", $element->getContent());
        $this->assertEquals(new EmptyAttributes(), $element->getAttributes());
        $this->assertSame([$validationMessage], $element->getValidation());
    }

    public function testSetters(): void
    {
        $validationMessage = $this->createMock(ValidationMessage::class);
        $element = new ListMemberElement(
            "test-id",
            "test-content",
            new EmptyAttributes(),
            [$validationMessage]
        );

        $validationMessage2 = $this->createMock(ValidationMessage::class);
        $element->setIdentifier("new-id");
        $element->setContent("new-content");
        $element->setAttributes(new EmptyAttributes());
        $element->setValidation([$validationMessage2]);

        $this->assertSame("new-id", $element->getIdentifier());
        $this->assertSame("new-content", $element->getContent());
        $this->assertEquals(new EmptyAttributes(), $element->getAttributes());
        $this->assertSame([$validationMessage2], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidation(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);

        $element = new ListMemberElement(
            "test-id",
            null,
            $this->createMock(EmptyAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidation($validationMessageTwo);

        $this->assertSame([$validationMessageOne, $validationMessageTwo], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidations(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);
        $validationMessageThree = $this->createMock(ValidationMessage::class);

        $element = new ListMemberElement(
            "test-id",
            null,
            $this->createMock(EmptyAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidations([$validationMessageTwo, $validationMessageThree]);

        $this->assertSame(
            [$validationMessageOne, $validationMessageTwo, $validationMessageThree],
            $element->getValidation()
        );
    }
}
