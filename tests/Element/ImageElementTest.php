<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Element;

use Exception;
use NLdoc\ElementStructure\Types\Attribute\HeadingElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\ImageElementAttributes;
use NLdoc\ElementStructure\Types\Element\HeadingElement;
use NLdoc\ElementStructure\Types\Element\ImageElement;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(ImageElement::class)]
class ImageElementTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructing(): void
    {
        $attributes = $this->createMock(ImageElementAttributes::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new ImageElement("test-id", $attributes, [$validationMessage]);
        $this->assertSame("test-id", $element->getIdentifier());
        $this->assertSame($attributes, $element->getAttributes());
        $this->assertSame([$validationMessage], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $attributes = $this->createMock(ImageElementAttributes::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new ImageElement("test-id", $attributes, [$validationMessage]);

        $attributes2 = $this->createMock(ImageElementAttributes::class);
        $validationMessage2 = $this->createMock(ValidationMessage::class);
        $element->setIdentifier("new-id");
        $element->setAttributes($attributes2);
        $element->setValidation([$validationMessage2]);

        $this->assertSame("new-id", $element->getIdentifier());
        $this->assertSame($attributes2, $element->getAttributes());
        $this->assertSame([$validationMessage2], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidation(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);

        $element = new ImageElement(
            "test-id",
            $this->createMock(ImageElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidation($validationMessageTwo);

        $this->assertSame([$validationMessageOne, $validationMessageTwo], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidations(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);
        $validationMessageThree = $this->createMock(ValidationMessage::class);

        $element = new ImageElement(
            "test-id",
            $this->createMock(ImageElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidations([$validationMessageTwo, $validationMessageThree]);

        $this->assertSame(
            [$validationMessageOne, $validationMessageTwo, $validationMessageThree],
            $element->getValidation()
        );
    }
}
