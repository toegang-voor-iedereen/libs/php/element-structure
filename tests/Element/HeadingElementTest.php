<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\DocumentElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\HeadingElementAttributes;
use NLdoc\ElementStructure\Types\Element\DocumentElement;
use NLdoc\ElementStructure\Types\Element\HeadingElement;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use PHPUnit\Framework\Attributes\CoversClass;
use Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(HeadingElement::class)]
class HeadingElementTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructing(): void
    {
        $attributes = $this->createMock(HeadingElementAttributes::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new HeadingElement(
            "test-id",
            "test-content",
            $attributes,
            [$validationMessage]
        );
        $this->assertSame("test-id", $element->getIdentifier());
        $this->assertSame("test-content", $element->getContent());
        $this->assertSame($attributes, $element->getAttributes());
        $this->assertSame([$validationMessage], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $attributes = $this->createMock(HeadingElementAttributes::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new HeadingElement(
            "test-id",
            "test-content",
            $attributes,
            [$validationMessage]
        );

        $attributes2 = $this->createMock(HeadingElementAttributes::class);
        $validationMessage2 = $this->createMock(ValidationMessage::class);
        $element->setIdentifier("new-id");
        $element->setContent("new-content");
        $element->setAttributes($attributes2);
        $element->setValidation([$validationMessage2]);

        $this->assertSame("new-id", $element->getIdentifier());
        $this->assertSame("new-content", $element->getContent());
        $this->assertSame($attributes2, $element->getAttributes());
        $this->assertSame([$validationMessage2], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidation(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);

        $element = new HeadingElement(
            "test-id",
            null,
            $this->createMock(HeadingElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidation($validationMessageTwo);

        $this->assertSame([$validationMessageOne, $validationMessageTwo], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidations(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);
        $validationMessageThree = $this->createMock(ValidationMessage::class);

        $element = new HeadingElement(
            "test-id",
            null,
            $this->createMock(HeadingElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidations([$validationMessageTwo, $validationMessageThree]);

        $this->assertSame(
            [$validationMessageOne, $validationMessageTwo, $validationMessageThree],
            $element->getValidation()
        );
    }
}
