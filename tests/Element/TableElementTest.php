<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\EmptyAttributes;
use NLdoc\ElementStructure\Types\Element\TableElement;
use NLdoc\ElementStructure\Types\Element\TextElement;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(TableElement::class)]
class TableElementTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructing(): void
    {
        $validationMessage = $this->createMock(ValidationMessage::class);
        $element = new TableElement(
            "test-id",
            "test-content",
            new EmptyAttributes(),
            [$validationMessage]
        );
        $this->assertSame("test-id", $element->getIdentifier());
        $this->assertSame("test-content", $element->getContent());
        $this->assertEquals(new EmptyAttributes(), $element->getAttributes());
        $this->assertSame([$validationMessage], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $validationMessage = $this->createMock(ValidationMessage::class);
        $element = new TableElement(
            "test-id",
            "test-content",
            new EmptyAttributes(),
            [$validationMessage]
        );

        $validationMessage2 = $this->createMock(ValidationMessage::class);
        $element->setIdentifier("new-id");
        $element->setContent("new-content");
        $element->setAttributes(new EmptyAttributes());
        $element->setValidation([$validationMessage2]);

        $this->assertSame("new-id", $element->getIdentifier());
        $this->assertSame("new-content", $element->getContent());
        $this->assertEquals(new EmptyAttributes(), $element->getAttributes());
        $this->assertSame([$validationMessage2], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testAddValidation(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);

        $element = new TableElement(
            "test-id",
            null,
            $this->createMock(EmptyAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidation($validationMessageTwo);

        $this->assertSame([$validationMessageOne, $validationMessageTwo], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testAddValidations(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);
        $validationMessageThree = $this->createMock(ValidationMessage::class);

        $element = new TableElement(
            "test-id",
            null,
            $this->createMock(EmptyAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidations([$validationMessageTwo, $validationMessageThree]);

        $this->assertSame(
            [$validationMessageOne, $validationMessageTwo, $validationMessageThree],
            $element->getValidation()
        );
    }
}
