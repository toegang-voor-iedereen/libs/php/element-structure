<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Element;

use NLdoc\ElementStructure\Types\Attribute\DocumentElementAttributes;
use NLdoc\ElementStructure\Types\Element\DocumentElement;
use NLdoc\ElementStructure\Types\Element\RootElementInterface;
use NLdoc\ElementStructure\Types\Element\TextElement;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use PHPUnit\Framework\Attributes\CoversClass;
use Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(DocumentElement::class)]
class DocumentElementTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructing(): void
    {
        $attributes = $this->createMock(DocumentElementAttributes::class);
        $member = $this->createMock(TextElement::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new DocumentElement(
            "test-id",
            [$member],
            $attributes,
            [$validationMessage]
        );
        $this->assertSame("test-id", $element->getIdentifier());
        $this->assertSame([$member], $element->getMembers());
        $this->assertSame($attributes, $element->getAttributes());
        $this->assertSame([$validationMessage], $element->getValidation());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $attributes = $this->createMock(DocumentElementAttributes::class);
        $member = $this->createMock(TextElement::class);
        $validationMessage = $this->createMock(ValidationMessage::class);

        $element = new DocumentElement(
            "test-id",
            [$member],
            $attributes,
            [$validationMessage]
        );

        $attributes2 = $this->createMock(DocumentElementAttributes::class);
        $member2 = $this->createMock(TextElement::class);
        $validationMessage2 = $this->createMock(ValidationMessage::class);
        $element->setIdentifier("new-id");
        $element->setMembers([$member2]);
        $element->setAttributes($attributes2);
        $element->setValidation([$validationMessage2]);

        $this->assertSame("new-id", $element->getIdentifier());
        $this->assertSame($attributes2, $element->getAttributes());
        $this->assertSame([$member2], $element->getMembers());
        $this->assertSame([$validationMessage2], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidation(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);

        $element = new DocumentElement(
            "test-id",
            [],
            $this->createMock(DocumentElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidation($validationMessageTwo);

        $this->assertSame([$validationMessageOne, $validationMessageTwo], $element->getValidation());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddValidations(): void
    {
        $validationMessageOne = $this->createMock(ValidationMessage::class);
        $validationMessageTwo = $this->createMock(ValidationMessage::class);
        $validationMessageThree = $this->createMock(ValidationMessage::class);

        $element = new DocumentElement(
            "test-id",
            [],
            $this->createMock(DocumentElementAttributes::class),
            [$validationMessageOne]
        );

        $this->assertSame([$validationMessageOne], $element->getValidation());

        $element->addValidations([$validationMessageTwo, $validationMessageThree]);

        $this->assertSame(
            [$validationMessageOne, $validationMessageTwo, $validationMessageThree],
            $element->getValidation()
        );
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddMember(): void
    {
        $memberOne = $this->createMock(RootElementInterface::class);
        $memberTwo = $this->createMock(RootElementInterface::class);

        $element = new DocumentElement(
            "test-id",
            [$memberOne],
            $this->createMock(DocumentElementAttributes::class),
            []
        );

        $this->assertSame([$memberOne], $element->getMembers());

        $element->addMember($memberTwo);

        $this->assertSame([$memberOne, $memberTwo], $element->getMembers());
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAddMembers(): void
    {
        $memberOne = $this->createMock(RootElementInterface::class);
        $memberTwo = $this->createMock(RootElementInterface::class);
        $memberThree = $this->createMock(RootElementInterface::class);

        $element = new DocumentElement(
            "test-id",
            [$memberOne],
            $this->createMock(DocumentElementAttributes::class),
            []
        );

        $this->assertSame([$memberOne], $element->getMembers());

        $element->addMembers([$memberTwo, $memberThree]);

        $this->assertSame(
            [$memberOne, $memberTwo, $memberThree],
            $element->getMembers()
        );
    }
}
