<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Attribute;

use NLdoc\ElementStructure\Types\Attribute\UnorderedListElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\UnorderedListStyleType;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(UnorderedListElementAttributes::class)]
class UnorderedListElementAttributesTest extends TestCase
{
    public function testConstructing(): void
    {
        $attributes = new UnorderedListElementAttributes(UnorderedListStyleType::Disc);
        $this->assertEquals(UnorderedListStyleType::Disc, $attributes->getStyleType());
    }

    public function testSetters(): void
    {
        $attributes = new UnorderedListElementAttributes(UnorderedListStyleType::Disc);
        $attributes->setStyleType(UnorderedListStyleType::Square);
        $this->assertEquals(UnorderedListStyleType::Square, $attributes->getStyleType());
    }
}
