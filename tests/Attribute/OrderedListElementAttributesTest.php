<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Attribute;

use NLdoc\ElementStructure\Types\Attribute\OrderedListElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\OrderedListStyleType;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(OrderedListElementAttributes::class)]
class OrderedListElementAttributesTest extends TestCase
{
    public function testConstructing(): void
    {
        $attributes = new OrderedListElementAttributes(OrderedListStyleType::Decimal, 1, false);
        $this->assertEquals(OrderedListStyleType::Decimal, $attributes->getStyleType());
        $this->assertEquals(1, $attributes->getStart());
        $this->assertEquals(false, $attributes->isReversed());
    }

    public function testSetters(): void
    {
        $attributes = new OrderedListElementAttributes(OrderedListStyleType::Decimal, 1, false);
        $attributes->setStyleType(OrderedListStyleType::AlphaLower);
        $attributes->setStart(1337);
        $attributes->setReversed(true);
        $this->assertEquals(OrderedListStyleType::AlphaLower, $attributes->getStyleType());
        $this->assertEquals(1337, $attributes->getStart());
        $this->assertEquals(true, $attributes->isReversed());
    }
}
