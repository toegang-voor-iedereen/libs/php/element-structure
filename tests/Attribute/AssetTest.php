<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Attribute;

use NLdoc\ElementStructure\Types\Attribute\Asset;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(Asset::class)]
class AssetTest extends TestCase
{
    public function testConstructing(): void
    {
        $asset = new Asset("hello.txt", "/loc/on/s3");

        $this->assertEquals("hello.txt", $asset->getFilename());
        $this->assertEquals("/loc/on/s3", $asset->getContentLocation());
    }

    public function testSetters(): void
    {
        $asset = new Asset("hello.txt", "something");

        $asset->setFilename("filename");
        $asset->setContentLocation("content-location");

        $this->assertEquals("filename", $asset->getFilename());
        $this->assertEquals("content-location", $asset->getContentLocation());
    }
}
