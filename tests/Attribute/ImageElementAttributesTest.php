<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Attribute;

use NLdoc\ElementStructure\Types\Attribute\ImageElementAttributes;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(ImageElementAttributes::class)]
class ImageElementAttributesTest extends TestCase
{
    public function testConstructing(): void
    {
        $attributes = new ImageElementAttributes("test-src", "test-alt");
        $this->assertEquals("test-src", $attributes->getSrc());
        $this->assertEquals("test-alt", $attributes->getAlt());
    }

    public function testSetters(): void
    {
        $attributes = new ImageElementAttributes("test-src", "test-alt");
        $attributes->setSrc("new-src");
        $attributes->setAlt("new-alt");
        $this->assertEquals("new-src", $attributes->getSrc());
        $this->assertEquals("new-alt", $attributes->getAlt());
    }
}
