<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Attribute;

use NLdoc\ElementStructure\Types\Attribute\Asset;
use NLdoc\ElementStructure\Types\Attribute\DocumentElementAttributes;
use PHPUnit\Framework\Attributes\CoversClass;
use Exception;
use PHPUnit\Framework\TestCase;

#[CoversClass(DocumentElementAttributes::class)]
class DocumentElementAttributesTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testConstructing(): void
    {
        $asset = $this->createMock(Asset::class);
        $attributes = new DocumentElementAttributes(
            "test-title",
            "test-subtitle",
            "test-title-prefix",
            "test-language",
            "test-direction",
            "test-category",
            "test-date",
            ["test-keyword"],
            ["test-author"],
            [$asset]
        );

        $this->assertSame([$asset], $attributes->getAssets());
        $this->assertSame("test-title", $attributes->getTitle());
        $this->assertSame("test-subtitle", $attributes->getSubtitle());
        $this->assertSame("test-title-prefix", $attributes->getTitlePrefix());
        $this->assertSame("test-language", $attributes->getLanguage());
        $this->assertSame("test-direction", $attributes->getDirection());
        $this->assertSame("test-category", $attributes->getCategory());
        $this->assertSame(["test-keyword"], $attributes->getKeywords());
        $this->assertSame(["test-author"], $attributes->getAuthors());
        $this->assertSame("test-date", $attributes->getDate());
    }

    /**
     * @throws Exception
     */
    public function testSetters(): void
    {
        $asset = $this->createMock(Asset::class);
        $attributes = new DocumentElementAttributes(
            "test-title",
            "test-subtitle",
            "test-title-prefix",
            "test-language",
            "test-direction",
            "test-category",
            "test-date",
            ["test-keyword"],
            ["test-author"],
            [$asset],
        );

        $asset2 = $this->createMock(Asset::class);
        $attributes->setAssets([$asset2]);
        $attributes->setTitle("new-title");
        $attributes->setSubtitle("new-subtitle");
        $attributes->setTitlePrefix("new-title-prefix");
        $attributes->setLanguage("new-language");
        $attributes->setDirection("new-direction");
        $attributes->setCategory("new-category");
        $attributes->setKeywords(["new-keyword"]);
        $attributes->setAuthors(["new-author"]);
        $attributes->setDate("new-date");

        $this->assertSame([$asset2], $attributes->getAssets());
        $this->assertSame("new-title", $attributes->getTitle());
        $this->assertSame("new-subtitle", $attributes->getSubtitle());
        $this->assertSame("new-title-prefix", $attributes->getTitlePrefix());
        $this->assertSame("new-language", $attributes->getLanguage());
        $this->assertSame("new-direction", $attributes->getDirection());
        $this->assertSame("new-category", $attributes->getCategory());
        $this->assertSame(["new-keyword"], $attributes->getKeywords());
        $this->assertSame(["new-author"], $attributes->getAuthors());
        $this->assertSame("new-date", $attributes->getDate());
    }
}
