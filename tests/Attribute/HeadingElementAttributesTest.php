<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Types\Attribute;

use NLdoc\ElementStructure\Types\Attribute\HeadingElementAttributes;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;

#[CoversClass(HeadingElementAttributes::class)]
class HeadingElementAttributesTest extends TestCase
{
    public function testConstructing(): void
    {
        $attributes = new HeadingElementAttributes(1);
        $this->assertEquals(1, $attributes->getLevel());
    }

    public function testSetters(): void
    {
        $attributes = new HeadingElementAttributes(1);
        $attributes->setLevel(2);
        $this->assertEquals(2, $attributes->getLevel());
    }
}
